program Acoustics
integer, parameter :: dp = SELECTED_REAL_KIND(14)
real(kind=dp),dimension(:,:,:),allocatable::p,v_x,v_y,v_z,p_new,v_x_new,v_y_new,v_z_new
integer::i,j,k,n_tot,q,obs_x,obs_y,obs_w, obs_d, obs_h
real(kind=dp)::h,tau,x,y,c,rho,kappa,t,zn,alpha,alpha_obs,zn_obs
character(20)::filename

rho=1.2_dp
kappa=1.01E5_dp
c=343.2_dp
x=10.0_dp
y=x
q=100
h=x/q
tau=h/c/sqrt(3.0_dp)
n_tot=300
alpha=0.9_dp
zn=rho*c*(1.0_dp+sqrt(1.0_dp-alpha))/(1.0_dp-sqrt(1.0_dp-alpha))
alpha_obs=0.8_dp
zn_obs=rho*c*(1.0_dp+sqrt(1.0_dp-alpha_obs))/(1.0_dp-sqrt(1.0_dp-alpha_obs))

obs_x=q/2
obs_y=22
obs_w = 100
obs_d = 6
obs_h = 18

allocate(p(q,q,q),v_x(q+1,q,q),v_y(q,q+1,q),v_z(q,q,q+1),p_new(q,q,q),&
        v_x_new(q+1,q,q),v_y_new(q,q+1,q),v_z_new(q,q,q+1))

p=0.0_dp
v_x=0.0_dp
v_y=0.0_dp
v_z=0.0_dp

do n=0,n_tot
    t=n*tau
    do i=1,q
        do j=1,q
            do k=1,q
                p_new(i,j,k)=p(i,j,k)-(kappa*tau/h)*(v_x(i+1,j,k)-v_x(i,j,k)+&
                                v_y(i,j+1,k)-v_y(i,j,k)+v_z(i,j,k+1)-v_z(i,j,k))
            end do  
        end do
    end do
        p_new(q/2,q*0.95,q/2)=0.08_dp*sin(2.0_dp*3.14159_dp*500*t)

    do i=2,q
        do j=2,q
            do k=2,q

                if ((i>=obs_x-obs_w/2).and.(i<=obs_x+obs_w/2).and.(j>=obs_y-obs_d/2)&
                                            .and.(j<=obs_y+obs_d/2).and.(k<=obs_h)) then
                    if (i==obs_x-obs_w/2) then
                        v_x_new(i,j,k)=p(i-1,j,k)/zn_obs
                        v_y_new(i,j,k)=0.0_dp
                        v_z_new(i,j,k)=0.0_dp
                    end if
                    if (i==obs_x+obs_w/2) then
                        v_x_new(i,j,k)=-p(i,j,k)/zn_obs
                        v_y_new(i,j,k)=0.0_dp
                        v_z_new(i,j,k)=0.0_dp
                   end if
				   
				   if (j==obs_y-obs_d/2) then
                        v_x_new(i,j,k)=0.0_dp
                        v_y_new(i,j,k)=p(i,j-1,k)/zn_obs
                        v_z_new(i,j,k)=0.0_dp
                    end if
                    if (j==obs_y+obs_d/2) then
                        v_x_new(i,j,k)=0.0_dp
                        v_y_new(i,j,k)=-p(i,j,k)/zn_obs
                        v_z_new(i,j,k)=0.0_dp
                   end if
				   if (k==obs_h) then
						v_x_new(i,j,k)=0.0_dp
                        v_y_new(i,j,k)=0.0_dp
                        v_z_new(i,j,k)=-p(i,j,k)/zn_obs
				   end if

				else
					v_x_new(i,j,k)=v_x(i,j,k)-(tau/rho/h)*(p_new(i,j,k)-p_new(i-1,j,k))
					v_y_new(i,j,k)=v_y(i,j,k)-(tau/rho/h)*(p_new(i,j,k)-p_new(i,j-1,k))
					v_z_new(i,j,k)=v_z(i,j,k)-(tau/rho/h)*(p_new(i,j,k)-p_new(i,j,k-1))
                end if
 
            end do
        end do
    end do

    do j=1,q
        do k=1,q
            v_x_new(1,j,k)=-p_new(1,j,k)/zn
            v_x_new(q+1,j,k)=p_new(q,j,k)/zn
        end do
    end do
    do i=1,q
        do k=1,q
            v_y_new(i,1,k)=-p_new(i,1,k)/zn
            v_y_new(i,q+1,k)=p_new(i,q,k)/zn
        end do
    end do
    do i=1,q
        do j=1,q
            v_z_new(i,j,1)=-p_new(i,j,1)/zn
            v_z_new(i,j,q+1)=p_new(i,j,q)/zn
        end do
    end do
           
    p=p_new
    v_x=v_x_new
    v_y=v_y_new
    v_z=v_z_new
    if((0<=n).and.(n<10)) then
        write(filename,fmt="(I1,a)"), n, ".dat"
    end if
    if((10<=n).and.(n<100)) then
         write(filename,fmt="(I2,a)"), n, ".dat"
    end if
    if((100<=n).and.(n<1000)) then
        write(filename,fmt="(I3,a)"), n, ".dat"
    end if
    open(unit=666,file=filename,action="write",status="replace")
    do i=1,q
        do j=1,q
            do k=17,17
                write(unit=666,fmt="(4f25.6)"),i*h, j*h, k*h, p(i,j,k)
            end do
        end do
    end do
end do
end program Acoustics
