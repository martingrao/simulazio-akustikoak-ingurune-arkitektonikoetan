# Simulazio akustikoak ingurune arkitektonikoetan
Gordailu honetan Martin Graok egindako Fisikako Graduaren GrAL-ean idatzitako kodea dago.

Hemen azaltzen den kodea GrAL-ean proposatutako egoera konkretu bakoitzera egokitu da.

Lan hau CC BY-NC-SA 4.0 lizentziaren bidez babestua dago: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en

